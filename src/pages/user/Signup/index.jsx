import { LockOutlined, MobileOutlined, UserOutlined, MailOutlined } from '@ant-design/icons';
import { Alert, message } from 'antd';
import React, { useState, useEffect } from 'react';
import ProForm, { ProFormText } from '@ant-design/pro-form';
import { useIntl, Link, history, FormattedMessage, SelectLang } from 'umi';
import Footer from '@/components/Footer';
import AccountModel from '@/models/account';
import styles from './index.less';
const signup = AccountModel.signup;
const getPlatformInfo = AccountModel.getPlatformInfo;
const LoginMessage = ({ content }) => (
  <Alert
    style={{
      marginBottom: 24,
    }}
    message={content}
    type="error"
    showIcon
  />
);

const Signup = () => {
  const [submitting, setSubmitting] = useState(false);
  const [signupState, setSignupState] = useState({});
  const intl = useIntl();

  let getPlatformInfoData;

  useEffect(async () => {
    getPlatformInfoData = await getPlatformInfo();
  }, []);

  const handleSubmit = async (values) => {
    setSubmitting(true);

    try {
      // 注册
      const msg = await signup({ ...values, type: 0 });
      console.log('Signup result ===>');
      console.log(msg);
      if (msg.status.message === 'SUCCESS') {
        const defaultSignupSuccessMessage = intl.formatMessage({
          id: 'pages.signup.success',
          defaultMessage: '注册成功!',
        });
        message.success(defaultSignupSuccessMessage);
        /** 此方法会跳转到 redirect 参数所在的位置 */

        if (!history) return;
        const { query } = history.location;
        const { redirect } = query;
        if (!redirect) {
          window.history.back();
        } else {
          window.location.href = redirect;
        }
      } // 如果失败去设置用户错误信息

      setSignupState(msg);
    } catch (error) {
      console.log(error);
      const defaultSignupFailureMessage = intl.formatMessage({
        id: 'pages.signup.failure',
        defaultMessage: '注册失败，请重试!',
      });
      message.error(defaultSignupFailureMessage);
    }

    setSubmitting(false);
  };

  const { status } = signupState;
  return (
    <div className={styles.container}>
      <div className={styles.lang} data-lang>
        {SelectLang && <SelectLang />}
      </div>
      <div className={styles.content}>
        {/* <div className={styles.top}>
          <div className={styles.header}>
            <Link to="/">
              <img alt="logo" className={styles.logo} src="/logo.svg" />
              <span className={styles.title}>物联网平台</span>
            </Link>
          </div>
          <div className={styles.desc}>
            {intl.formatMessage({
              id: 'pages.layouts.userLayout.title',
            })}
          </div>
        </div> */}

        <div className={styles.main}>
          <ProForm
            initialValues={{
              autoLogin: true,
            }}
            submitter={{
              searchConfig: {
                submitText: intl.formatMessage({
                  id: 'pages.signup.submit',
                  defaultMessage: '注册',
                }),
              },
              render: (_, dom) => dom.pop(),
              submitButtonProps: {
                loading: submitting,
                size: 'large',
                style: {
                  width: '100%',
                },
              },
            }}
            onFinish={async (values) => {
              console.log(values);
              handleSubmit(values);
            }}
          >
            {status === 'error' && (
              <LoginMessage
                content={intl.formatMessage({
                  id: 'pages.signup.errorMessage',
                  defaultMessage: '注册失败',
                })}
              />
            )}
            <>
              <ProFormText
                name="username"
                fieldProps={{
                  size: 'large',
                  prefix: <UserOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.username.placeholder',
                  defaultMessage: '请输入用户名',
                })}
                rules={[
                  {
                    required: true,
                    message: (
                      <FormattedMessage
                        id="pages.signup.username.required"
                        defaultMessage="请输入用户名!"
                      />
                    ),
                  },
                ]}
              />

              <ProFormText
                name="nickName"
                fieldProps={{
                  size: 'large',
                  prefix: <UserOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.nickname.placeholder',
                  defaultMessage: '请输入昵称',
                })}
              />

              <ProFormText
                name="phoneNumber"
                fieldProps={{
                  size: 'large',
                  prefix: <MobileOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.mobile.placeholder',
                  defaultMessage: '请输入手机号',
                })}
                rules={[
                  {
                    pattern: /^1\d{10}$/,
                    message: (
                      <FormattedMessage
                        id="pages.signup.phoneNumber.invalid"
                        defaultMessage="手机号格式错误！"
                      />
                    ),
                  },
                ]}
              />

              <ProFormText
                name="email"
                fieldProps={{
                  size: 'large',
                  prefix: <MailOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.email.placeholder',
                  defaultMessage: '请输入邮箱',
                })}
                rules={[
                  {
                    pattern: /^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-])+/,
                    message: (
                      <FormattedMessage
                        id="pages.signup.email.invalid"
                        defaultMessage="请输入正确的邮箱格式!"
                      />
                    ),
                  },
                ]}
              />

              <ProFormText.Password
                name="password"
                fieldProps={{
                  size: 'large',
                  prefix: <LockOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.password.placeholder',
                  defaultMessage: '请输入密码',
                })}
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value && getFieldValue('username')) {
                        return Promise.reject();
                      } else {
                        return Promise.resolve();
                      }
                    },
                    message: (
                      <FormattedMessage
                        id="pages.signup.password.required"
                        defaultMessage="输入用户名情况下请输入密码!"
                      />
                    ),
                  }),
                  {
                    pattern: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
                    message: (
                      <FormattedMessage
                        id="pages.signup.password.invalid"
                        defaultMessage="密码格式错误！"
                      />
                    ),
                  },
                ]}
              />

              <ProFormText.Password
                name="repassword"
                fieldProps={{
                  size: 'large',
                  prefix: <LockOutlined className={styles.prefixIcon} />,
                }}
                placeholder={intl.formatMessage({
                  id: 'pages.signup.repassword.placeholder',
                  defaultMessage: '请再次输入密码',
                })}
                rules={[
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (
                        (!value && getFieldValue('password')) ||
                        (!!value && getFieldValue('password') != value)
                      ) {
                        return Promise.reject();
                      } else {
                        return Promise.resolve();
                      }
                    },
                    message: (
                      <FormattedMessage
                        id="pages.signup.repassword.required"
                        defaultMessage="请输入正确密码!"
                      />
                    ),
                  }),
                ]}
              />
            </>
          </ProForm>
        </div>
      </div>
      <Footer />
    </div>
  );
};

export default Signup;
